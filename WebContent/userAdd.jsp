<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add user...</title>
</head>
<body>
	<s:form action="adduser">
		<s:textfield name="user.name" label="User"></s:textfield>
		<s:textfield name="user.username" label="UserName"></s:textfield>
		<s:password name="user.password" label="PassWord"></s:password>
		<s:submit value="Add User" align="center"></s:submit>
	</s:form>
</body>
</html>