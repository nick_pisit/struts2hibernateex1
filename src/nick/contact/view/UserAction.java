package nick.contact.view;

import java.util.List;
import nick.contact.controller.UserManager;
import nick.contact.model.User;
import com.opensymphony.xwork2.*;

public class UserAction extends ActionSupport{
	private User user;
	private UserManager userManager;
	
	public UserAction() {
		userManager = new UserManager();
	}
	public String execute(){
		return SUCCESS;
	}
	public String add(){
		try{
			userManager.addUser(getUser());
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return 	SUCCESS;	
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
