package nick.contact.controller;

import java.util.List;
import org.hibernate.HibernateException; 
import org.hibernate.Session;
import nick.contact.model.User;
import nick.contact.util.HibernateUtil;

public class UserManager extends HibernateUtil{
	public User addUser(User user){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		return user;
	}
}
